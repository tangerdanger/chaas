# Package

version       = "0.1.0"
author        = "Ryan Cotter"
description   = "Cheese As A Service"
license       = "MIT"
srcDir        = "src"
installExt    = @["nim"]
bin           = @["chaas"]


# Dependencies

requires "nim >= 0.19.4"
requires "jester#head"
requires "karax#head"

task run, "Serve the page":
  exec("mkdir -p public")
  exec("nim js --nimcache:public src/chaas.nim")
  exec("nim c -r --out:main src/main.nim")
