include karax/prelude

proc renderCheeseForm*(onSubmit: EventHandler): VNode =
  buildHtml(tdiv):
    input(id = "submittedBy", class = "new-cheese-input", placeholder = "Your Name", name = "newCheeseName")
    textarea(id = "cheeseMessage", class = "new-cheese-ta", placeholder = "Say something cheesy!", name = "newCheeseText")
    button(onclick = onSubmit):
      text "Submit Me"
