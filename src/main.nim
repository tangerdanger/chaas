import jester, os, cgi, times, tables, strutils, strformat, strtabs, asyncdispatch, json, db_sqlite

var 
  khtml: string

proc initDatabase(): DBConn

var db = initDatabase()

from chaas import Cheese

proc initDatabase(): DBConn =
  result = open("chaas.db", "chaas", "chaas", "chaas")

  try:
    result.exec(sql("""
      CREATE TABLE chaas_cheeseboard (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      submittedby VARCHAR(254) not null,
      cheesemessage TEXT not null,
      submittedon TEXT not null
      )"""
    ))
  except DbError:
    let e = getCurrentExceptionMsg()
    #Table exists
    echo "Table exists"

proc submitMessage(submittedBy, cheeseMessage: string): Future[int64] {.async.} =
  return tryInsertID(db, sql"""
    INSERT INTO chaas_cheeseboard(submittedby, cheesemessage, submittedon)
    VALUES (?, ?, DATETIME('now'))
    """, submittedBy, cheeseMessage)

routes:

  post "/boards.json":
    var
      start = getInt(%@"start", 0)
      count = getInt(%@"count", 30)
    const cheeseQuery =
      sql"""
        select submittedby, cheesemessage, submittedon
        from chaas_cheeseboard 
        order by submittedon asc limit ?, ?;
      """
    let cheeseCount = getValue(db, sql"select count(*) from chaas_cheeseboard").parseInt()
    let moreCount = max(0, cheeseCount - (start + count))

    var list: seq[Cheese] = @[]
    for data in getAllRows(db, cheeseQuery, start, count):
      list.add(Cheese(
        submittedBy: data[0],
        cheeseMessage: data[1],
        submittedOn: data[2]
      ))

    resp $(%list), "application/json"

  post "/submit":
    let 
      body = parseJson(request.body)
      submittedBy = body["submittedby"].getStr
      cheeseMessage = body["cheesemessage"].getStr
    if submittedBy.len <= 0 or cheeseMessage.len <= 0:
      resp Http400, "Invalid data"
    let rowId = await submitMessage(submittedBy, cheeseMessage)
    echo rowId

    let row = db.getRow(sql"""SELECT submittedby, cheesemessage, submittedon FROM chaas_cheeseboard WHERE id = ?""", $rowId)
    let retVal = """{"submittedBy": "$#", "cheeseMessage": "$#", "submittedOn": "$#"}""" % [row[0], row[1], row[2]]

    resp retVal, "application/json"

  get "/":
    khtml = readFile(getStaticDir(request) / "chaas.html") % 
      {
        "title": "Chaas"
      }.newStringTable()
    resp khtml

runForever()
