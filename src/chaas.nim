# This is just an example to get you started. A typical hybrid package
# uses this file as the main entry point of the application.
import times, strformat, strutils, options, json

from db_sqlite import Row

type
  Cheese* = object
    submittedBy*: string
    cheeseMessage*: string
    submittedOn*: string

when defined(js):
  include karax/prelude
  import sugar, karax/kajax, karax/vdom, karax/kdom

  import components/cheeseform

  type
    State = object
      cheeselist*: seq[Cheese]
      moreCount*: int
      loading: bool

  func initCheese*(): Cheese =
    result.submittedBy = ""
    result.cheeseMessage = ""
    result.submittedOn = ""

  func initState(): State =
    result.cheeselist = @[]
    result.moreCount = 0

  proc render*(cheese: Cheese): VNode =
    buildHtml(tdiv):
      tdiv:
        h3:
          text fmt"{cheese.submittedBy} says:"
        p:
          text cheese.cheeseMessage

  proc render*(cheeselist: seq[Cheese]): VNode =
    buildHtml(tdiv):
      if cheeselist.len > 0:
        for cb in cheeselist.items:
          render cb

  var state = initState()

  proc makeFooter(): VNode =
    result = buildHtml(footer(class = "footer")):
      p:
        text "Trademark ChaasCo (🧀)"

  proc makeHeader(): VNode = 
    ##[
    ## Helper for creating header html
    ]##

    result = buildHtml(header(class = "header")):
      h1:
        text "CHAAS"
      h2:
        text "Cheese As A Service"

  proc afterCheeseSubmit(httpStatus: int, response: kstring, state: var State) =
    ##[
    ## Response handler for after submitting a message
    ]##

    let 
      resp = parseJson($response)
      sby = resp["submittedBy"].getStr
      cm = resp["cheeseMessage"].getStr
      so = resp["submittedOn"].getStr
    state.cheeselist.add(Cheese(submittedBy: sby, cheeseMessage: cm, submittedOn: so))
    state.moreCount += 1
    document.getElementById("submittedBy").value = ""
    document.getElementById("cheeseMessage").value = ""

  proc onCheeseSubmit(event: Event, node: VNode, state: var State) = 
    ##[
    ## Handler for submitting a new cheese
    ]## 

    let
      name = document.getElementById("submittedBy")
      text = document.getElementById("cheeseMessage")
      data = """{"submittedby": "$#", "cheesemessage": "$#"}""" % [$name.value, $text.value]

    ajaxPost("/submit", @[], data, (s: int, r: kstring) => afterCheeseSubmit(s, r, state))


  proc loadBoards(httpStatus: int, response: kstring, state: var State) =
    ##[
    ## On success, load our cheese messages into our state
    ]##

    if httpStatus.status == Http200:
      let
        resp = parseJson($response)

      for row in resp.getElems.items:
        state.cheeselist.add(Cheese(submittedBy: row["submittedBy"].getStr, cheeseMessage: row["cheeseMessage"].getStr, submittedOn: row["submittedOn"].getStr))
        state.moreCount += 1

    if state.loading == true:
      state.loading = false

  proc onLoadMoreCheese(event: Event, node: VNode, state: var State) =
    ##[
    ## Handler for loading more cheeses
    ]## 

    let data = """{"start": $#, "count": $#}""" % [$state.moreCount, "10"]
    ajaxPost("/boards.json", @[], data, (s: int, r: kstring) => loadBoards(s, r, state))
    if state.loading != true:
      state.loading = true

  proc getBoards(start, count: int, state: var State) =
    ##[
    ## Get our board list from the database
    ]##

    let data = """{"start": $#, "count": $#}""" % [$start, $count]
    ajaxPost("/boards.json", @[], data, (s: int, r: kstring) => loadBoards(s, r, state))

  proc main() =
    if state.cheeselist.len == 0:
      getBoards(0, 10, state)

    proc createDom(data: RouterData): VNode =
      result = buildHtml(tdiv(class = "main-wrapper")):
        section(class = "chaas"):
          makeHeader()
          section(class = "main"):
            p:
              text "Finally, a useful service just for you! Cancel your other services, uninstall those apps and throw away that fridge, you won't need them any more!"
            p:
              text "It's just Cheese, but as a service"
            button:
              text "Give us money, now!"
          section(class = "cheese-form"):
            renderCheeseForm((e: Event, n: VNode) => onCheeseSubmit(e, n, state))
          if state.cheeselist.len > 0:
            section(id = "cheese-list", class = "cheese-list"):
              render state.cheeselist
              button(class = "load-more", onclick = (e: Event, n: VNode) => onLoadMoreCheese(e, n, state)):
                if state.loading == true:
                  text "Loading..."
                else:
                  text "Load More Cheese!"

          makeFooter()

    setRenderer createDom

  when isMainModule:
    main()
